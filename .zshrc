# ロケール
export LANG=ja_JP.UTF-8

# パス
export PATH=/opt/local/bin:/opt/local/sbin:/opt/local/apache2/bin:$PATH

JAVA_HOME=/Library/Java/JavaVirtualMachines/1.6.0_29-b11-402.jdk/Contents/Home
export JAVA_HOME
PATH=${JAVA_HOME}/bin:$PATH
export PATH

# エイリアス
alias ls='ls -FGv --color=auto'
alias h='history -E 1'
alias dmesg='dmesg -T'
alias catsys='cat /var/log/syslog'

# プロンプト
PROMPT='%F{green}%n%f%F{magenta}@%f%F{green}%m%f %# '
RPROMPT="[%~]"

# ヒストリ
HISTFILE=~/.zsh-history
HISTSIZE=10000
SAVEHIST=10000

# 補完
autoload -U compinit;compinit

# 矢印キー補完
zstyle ':completion:*:default' menu select=1

# タブキー連打で補完候補を順次表示
setopt auto_menu

# 補完候補一覧でファイルの種別を識別マーク表示
setopt list_types

# 履歴ファイルに時刻
setopt extended_history

# 上書きせずに追加
setopt append_history

# 直前と同じコマンドは追加しない
setopt hist_ignore_dups

# 重複したコマンドは追加しない
setopt hist_ignore_all_dups

# ヒストリから呼び出して一旦編集できる状態になる
setopt hist_verify

# 履歴の共有
setopt share_history

# cd と入力してTabキーで今までに移動したディレクトリを一覧表示
setopt auto_pushd

# ディレクトリスタックに重複する物は古い方を削除
setopt pushd_ignore_dups

# 入力ミス
setopt correct
